import firebase from 'firebase'
import firestore from 'firebase/firestore'

let config = {
  apiKey: "AIzaSyD0PxMn1dkBAM4icqCkpwmE4WwMvw54tU4",
  authDomain: "vue-live-chat.firebaseapp.com",
  databaseURL: "https://vue-live-chat.firebaseio.com",
  projectId: "vue-live-chat",
  storageBucket: "vue-live-chat.appspot.com",
  messagingSenderId: "157568854383"
};

const firebaseApp = firebase.initializeApp(config)
firebaseApp.firestore().settings({
  timestampsInSnapshots: true
})

export default firebaseApp.firestore()